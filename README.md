## Twitterparser

Example program to use [lbsntransform](https://lbsn.vgiscience.org/lbsntransform/) as a package, 
to process custom Twitter data.

This example program will  
- (1) connect to a remote server via ssh  
- (2) download json files (either plain or zipped) from a specific remote folder  
- (3) optionally extract zip file with Twitter jsons,  
- (4) and transform jsons to rawdb or hlldb (using lbsntransform).

Command line args specific to twitterparser are 
processed before handing the remaining arguments to lbsntransform.

Example:
```bash
twitterparser \
    --key_path ./ssh/privatekey \
    --username xyz \
    --remote_directory /files/remote/path \
    --server_address remoteserver.tld.com \
    # lbsntransform arguments below \
    --origin 3 \
    --file_input \
    --file_type "json" \
    # ...
```

> :notebook: **Order is not important**: You can mix arguments from `twitterparser` and
  `lbsntransform`. Any arguments not known by twitterparser will be forwarded to
  lbsntransform.

## Setup

1. Install dependencies using conda

```bash
git clone https://gitlab.vgiscience.de/ad/twitterparser.git
cd twitterparser
conda config --env --set channel_priority strict
conda env create -f environment.yml
conda activate twitterparser
```

2. Install twitterparser without dependencies

```bash
pip install --no-deps --upgrade .
```

For development setup, use:
```bash
pip install --no-deps --editable .
```