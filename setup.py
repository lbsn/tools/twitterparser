# -*- coding: utf-8 -*-

"""Setuptools config file
"""

import sys
from setuptools import setup, find_packages

with open('README.md') as f:
    LONG_DESCRIPTION = f.read()

try:
    from semantic_release import setup_hook
    setup_hook(sys.argv)
except ImportError:
    pass

VERSION_DICT = {}
with open("twitterparser/version.py") as fp:
    exec(fp.read(), VERSION_DICT)  # pylint: disable=W0122
VERSION = VERSION_DICT['__version__']

setup(name="twitterparser",
      version=VERSION,
      description="Example program to use lbsntransform "
                  "as a package, to process custom twitter data",
      long_description=LONG_DESCRIPTION,
      long_description_content_type='text/markdown',
      author='Alexander Dunkel',
      author_email='alexander.dunkel@tu-dresden.de',
      url='https://gitlab.vgiscience.de/ad/twitterparser',
      license='GNU GPLv3 or any higher',
      packages=find_packages(
          include=['twitterparser', 'twitterparser.*']),
      include_package_data=True,
      install_requires=[
          'lbsntransform',
          'lbsnstructure>=0.5.1',
          'protobuf',
          'psycopg2',
          'ppygis3',
          'shapely',
          'emoji',
          'requests',
          'geos',
          'numpy',
          'requests',
          'regex'
      ],
      extras_require={
          'nltk_stopwords':  [
              "nltk"  # python -c 'import nltk;nltk.download("stopwords")'
          ]
      },
      entry_points={
          'console_scripts': [
              'twitterparser = twitterparser.__main__:main'
          ]
      })
