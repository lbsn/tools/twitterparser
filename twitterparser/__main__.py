#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
twitterparser example package script to load
and transfer custom twitter data to
rawdb [1] or hlldb [2] using
lbsntransform [3].

Input arguments:
- server_address # remote server address to pull data from
- username # ssh username
- key_path # path to private ssh key
- passphrase # passphrase will be queried using getpass() on start
- remote_directory # remote directory on server
- zipped # whether remote json files must be unzipped first (Default: False)

Output arguments:
- additional lbsntransform arguments to convert and submit
  data

Zip file scheme:
- zip files will be extracted to a flat directory and
and any json in this directory will be parsed (and submitted)
by lbsntransform afterwards. Once extracted (or submitted),
zip files will be removed locally. Zip files on the remote
ssh server will not be removed.

[1]: https://gitlab.vgiscience.de/lbsn/databases/rawdb
[2]: https://gitlab.vgiscience.de/lbsn/databases/hlldb
[3]: https://gitlab.vgiscience.de/lbsn/lbsntransform
"""

__author__ = "Alexander Dunkel"
__license__ = "GNU GPLv3"
# version: see version.py

import logging
import sys
from pathlib import Path

from lbsntransform.config.config import BaseConfig as LBSNTBaseConfig
from lbsntransform.input.load_data import LoadData
from lbsntransform.lbsntransform_ import LBSNTransform
from lbsntransform.output.shared_structure import TimeMonitor
from lbsntransform.tools.helper_functions import HelperFunctions as HF

from twitterparser.config.config import BaseConfig
from twitterparser.tools.tools import Connection, Utils


def main():
    """ Main function for cli-mode to process data
    from remote ssh location to rawdb using lbsntransform
    for conversion to lbsnstructure.
    """
    # Load twitterparser config
    config = BaseConfig()
    config.init_workdir()
    # Parse args and return remaining lbsntransform args
    config.parse_args()
    # init logging
    logger = get_logger(config.debug)

    # Init lbsntransform
    lbsn_config = LBSNTBaseConfig()
    lbsn_config.parse_args(config.lbsntransform_args)
    importer = HF.load_importer_mapping_module(
        lbsn_config.origin, lbsn_config.mappings_path)
    lbsntransform = init_lbsntransform(lbsn_config, importer)

    # init sftp connection
    connection = Connection(
        server_address=config.server_address,
        username=config.username,
        key_path=config.key_path,
        passphrase=config.passphrase,
        remote_directory=config.remote_directory
    )

    # check for existing config file/previous run
    if config.remote_filelist is None:
        files = connection.get_filelist()
        config.set_remote_filelist(files)
        logger.info(
            f'Added {len(files)} files to the queue: \n{files}')
    else:
        logger.info(
            f'Loaded existing queue with {len(config.remote_filelist)} files')
    for filename in config.remote_filelist:
        if config.skip_filelist and filename in config.skip_filelist:
            logger.info(
                f'Skipping file: {filename}')
            continue
        file_local_path = Path(config.workdir / filename)
        # get files
        extracted_dir = Path(config.workdir / file_local_path.stem)
        if file_local_path.exists() or (config.zipped and extracted_dir.exists()):
            logger.info(
                f'File found: '
                f'{file_local_path.relative_to(file_local_path.parent.parent)}. '
                f'Continue processing')
        else:
            # get file from remote server
            connection.get_file(filename)
            # close connection afterwards
            connection.close()
            logger.info(
                f'Downloaded file: {filename} '
                f'({Utils.file_size_gb(file_local_path):.2f}GB)')
        # process data
        if config.zipped:
            # process json in local zipfiles
            _unzip_and_transform(
                extracted_dir, file_local_path,
                logger, lbsn_config, importer, lbsntransform)
        else:
            # process regular json (not zipped)
            lbsn_config.input_path = Path(config.workdir)
            input_data = init_loaddata(
                lbsn_config, importer, lbsntransform.cursor_input)
            # transfer data from folder using lbsntransform
            transform_data(input_data, lbsntransform, lbsn_config)
            # remove json file locally
            Utils.remove_local_file(file_local_path)
        # file processed, add to skip list
        config.update_skiplist(file_local_path.name)

    connection.close()
    lbsntransform.cursor_output.close()
    lbsntransform.close_log()
    logger.info(f'All done.')


if __name__ == "__main__":
    main()


def _unzip_and_transform(
        extracted_dir: Path, file_local_path: Path,
        logger, lbsn_config: BaseConfig, importer, lbsntransform):
    """Extract double zipped json, transfer, and remove temporary files and
    folders afterwards."""
    if not extracted_dir.exists():
        Utils.extract_file(file_local_path)
        # remove zip file locally to free up space
        Utils.remove_local_file(file_local_path)
    extracted_filelist = Utils.get_local_filelist(extracted_dir)
    logger.info(
        f'Processing {len(extracted_filelist)} extracted files in '
        f'{extracted_dir.relative_to(extracted_dir.parent.parent)}.')
    for zipfile in extracted_filelist:
        # process extracted zip files
        output_dir = extracted_dir / zipfile.stem
        Utils.extract_file(
            file=zipfile, output_dir=output_dir, no_folders=True)
        lbsn_config.input_path = output_dir
        input_data = init_loaddata(
            lbsn_config, importer, lbsntransform.cursor_input)
        # transfer data from folder using lbsntransform
        transform_data(input_data, lbsntransform, lbsn_config)
        # remove zip file locally
        Utils.remove_local_file(zipfile)
        Utils.remove_local_folder(output_dir)
        logger.info(
            f'Continuing with next zip file..\n')
    # remove empty folder
    extracted_dir.rmdir()


def transform_data(input_data: LoadData, lbsntransform, config):
    """Transform data in folder to output using lbsntransform

    This is directly taken from `lbsntransform.__main__`.

    Here, `record` is a single json structure (a python dictionary)
    that is given to lbsntransform for transformation (to lbsnstructure [1])
    and transmission (to a lbsn-raw or lbsn-hll database. Submission will
    be trigged automatically by lbsntransform after certain thresholds are
    passed (e.g. 50.000 records).

    [1]: https://gitlab.vgiscience.de/lbsn/structure/protobuf
    """
    with input_data as records:
        for record in records:
            lbsntransform.add_processed_records(record)
            # report progress
            if lbsntransform.processed_total % 1000 == 0:
                stats_str = HF.report_stats(
                    input_data.count_glob,
                    input_data.continue_number,
                    lbsntransform.lbsn_records)
                print(stats_str, end='\r')
                sys.stdout.flush()
    # store remaining, free up cache
    lbsntransform.store_lbsn_records()
    lbsntransform.log.info(
        f'\n\n{"".join([f"(Dry Run){chr(10)}" if config.dry_run else ""])}'
        f'Processed {input_data.count_glob} input records '
        f'(Input {input_data.start_number} to '
        f'{input_data.continue_number}). '
        f'\n\nIdentified {lbsntransform.processed_total} LBSN records, '
        f'with {lbsntransform.lbsn_records.count_glob_total} '
        f'distinct LBSN records overall. '
        f'{HF.get_skipped_report(input_data.import_mapper)}. '
        f'Merged {lbsntransform.lbsn_records.count_dup_merge_total} '
        f'duplicate records.')
    lbsntransform.log.info(
        f'\n{HF.get_count_stats(lbsntransform.lbsn_records)}')


def init_lbsntransform(config, importer):
    """Initialize lbsntransform

    This is directly taken from `lbsntransform.__main__`
    """
    return LBSNTransform(
        importer=importer,
        logging_level=config.logging_level,
        is_local_input=config.is_local_input,
        transfer_count=config.transfer_count,
        csv_output=config.csv_output,
        csv_suppress_linebreaks=config.csv_suppress_linebreaks,
        dbuser_output=config.dbuser_output,
        dbserveraddress_output=config.dbserveraddress_output,
        dbname_output=config.dbname_output,
        dbpassword_output=config.dbpassword_output,
        dbserverport_output=config.dbserverport_output,
        dbformat_output=config.dbformat_output,
        dbuser_input=config.dbuser_input,
        dbserveraddress_input=config.dbserveraddress_input,
        dbname_input=config.dbname_input,
        dbpassword_input=config.dbpassword_input,
        dbserverport_input=config.dbserverport_input,
        dbuser_hllworker=config.dbuser_hllworker,
        dbserveraddress_hllworker=config.dbserveraddress_hllworker,
        dbname_hllworker=config.dbname_hllworker,
        dbpassword_hllworker=config.dbpassword_hllworker,
        dbserverport_hllworker=config.dbserverport_hllworker,
        include_lbsn_bases=config.include_lbsn_bases,
        dry_run=config.dry_run,
        hmac_key=config.hmac_key)


def init_loaddata(config, importer, cursor_input):
    """Initialize lbsntransform load data

    This is directly taken from `lbsntransform.__main__`
    """
    return LoadData(
        importer=importer,
        is_local_input=config.is_local_input,
        startwith_db_rownumber=config.startwith_db_rownumber,
        skip_until_file=config.skip_until_file,
        cursor_input=cursor_input,
        input_path=config.input_path,
        recursive_load=config.recursive_load,
        local_file_type=config.local_file_type,
        endwith_db_rownumber=config.endwith_db_rownumber,
        is_stacked_json=config.is_stacked_json,
        is_line_separated_json=config.is_line_separated_json,
        csv_delim=config.csv_delim,
        use_csv_dictreader=config.use_csv_dictreader,
        input_lbsn_type=config.input_lbsn_type,
        dbformat_input=config.dbformat_input,
        geocode_locations=config.geocode_locations,
        ignore_input_source_list=config.ignore_input_source_list,
        disable_reactionpost_ref=config.disable_reactionpost_ref,
        map_relations=config.map_relations,
        transfer_reactions=config.transfer_reactions,
        ignore_non_geotagged=config.ignore_non_geotagged,
        min_geoaccuracy=config.min_geoaccuracy,
        source_web=config.source_web,
        skip_until_record=config.skip_until_record,
        zip_records=config.zip_records,
        include_lbsn_objects=config.include_lbsn_objects,
        override_lbsn_query_schema=config.override_lbsn_query_schema)


def get_logger(debug: bool = None):
    """Get logging handler"""
    level = logging.INFO
    if debug:
        level = logging.DEBUG
    logging.basicConfig(
        handlers=[logging.StreamHandler()],
        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
        datefmt='%H:%M:%S',
        level=level)
    # use lbsntransform logger
    log = logging.getLogger(__name__)
    return log
