# -*- coding: utf-8 -*-

"""
Collection of tools being used in twitterparser package.
"""

import sys
import enum
import os
import shutil
import logging
from pathlib import Path
from zipfile import ZipFile

from paramiko import AutoAddPolicy, SSHClient

WORKDIR: Path = Path.cwd() / "workdir"
LOG = logging.getLogger()


class SIZE_UNIT(enum.Enum):
    BYTES = 1
    KB = 2
    MB = 3
    GB = 4


class Utils():
    """Base class for additional utilities"""

    def convert_unit(size_in_bytes: int, unit: SIZE_UNIT):
        """Convert the size from bytes to other units like KB, MB or GB"""
        if unit == SIZE_UNIT.KB:
            return size_in_bytes/1024
        elif unit == SIZE_UNIT.MB:
            return size_in_bytes/(1024*1024)
        elif unit == SIZE_UNIT.GB:
            return size_in_bytes/(1024*1024*1024)
        else:
            return size_in_bytes

    def file_size_gb(file: Path) -> float:
        """Get the size of a file in GB"""
        return Utils.convert_unit(file.stat().st_size, SIZE_UNIT.GB)

    def extract_file(
            file: Path, output_dir: Path = None, no_folders: bool = None):
        """Extract file to output_dir

        Args: Optional specify no_folders to extract all json files in the main
        output_dir"""
        if output_dir is None:
            output_dir = WORKDIR / file.stem
        if output_dir.exists():
            LOG.info(f"Extracted files exist. Resuming processing..")
            return
        if no_folders:
            with ZipFile(file, 'r') as zip_ref:
                for z_name in zip_ref.namelist():
                    filename = os.path.basename(z_name)
                    # skip directories
                    if not filename:
                        continue
                # copy file (taken from zipfile's extract)
                LOG.info(
                    f"Extracting {z_name} to "
                    f"{output_dir.relative_to(output_dir.parent.parent)}.. "
                    f"({Utils.file_size_gb(file):.2f}GB)")
                source = zip_ref.open(z_name)
                output_dir.mkdir()
                target = open(output_dir / filename, "wb")
                with source, target:
                    shutil.copyfileobj(source, target)
            return
        with ZipFile(file, 'r') as zip_ref:
            LOG.info(
                f"Extracting {file.name} to "
                f"{output_dir.relative_to(output_dir.parent.parent)}.. "
                f"({Utils.file_size_gb(file):.2f}GB)")
            zip_ref.extractall(output_dir)

    @staticmethod
    def remove_local_file(file_path: Path):
        """Remove a local file"""
        if not file_path.exists():
            LOG.debug(
                f"Local file does not exist: "
                f"{file_path.relative_to(file_path.parent.parent)} ")
            return
        file_path.unlink()
        LOG.info(
            f"Removed processed file "
            f"{file_path.relative_to(file_path.parent.parent)} "
            f"locally")

    @staticmethod
    def get_local_filelist(directory_path: Path):
        """Get a sorted list of all files in a directory"""
        file_list = [elem for elem in directory_path.iterdir()
                     if elem.is_file()]
        file_list.sort()
        return file_list

    @staticmethod
    def remove_local_folder(file_path: Path):
        """Remove a local folder and all files"""
        if file_path.is_file():
            LOG.debug(
                f"{file_path.relative_to(file_path.parent.parent)} is a file (!). "
                f"Skipping removal.. ")
            return
        # remove all files
        for f in file_path.iterdir():
            if f.is_file():
                Utils.remove_local_file(f)
        # remove empty folder
        file_path.rmdir()
        LOG.info(
            f"Removed local processed folder "
            f"{file_path.relative_to(file_path.parent.parent)} ")


class Connection():
    """Base connection class for handling sftp"""

    def __init__(self,
                 server_address, username, key_path, passphrase, remote_directory):
        """Init connection
        """
        self.server_address = server_address
        self.username = username
        self.key_path = str(key_path)
        self.passphrase = passphrase
        self.remote_directory = remote_directory
        self.client = None
        self.sftp = None

    def connect(self):
        self.client = SSHClient()
        # automatically add keys without requiring human intervention
        self.client.set_missing_host_key_policy(AutoAddPolicy())
        self.client.connect(
            self.server_address, username=self.username,
            key_filename=self.key_path, passphrase=self.passphrase)
        self.sftp = self.client.open_sftp()
        self.sftp.chdir(self.remote_directory)

    def is_alive(self):
        """Reconnect required?"""
        return self.client.get_transport() is not None

    def close(self):
        """Close SSH and SFTP sessions"""
        try:
            if self.sftp:
                self.sftp.close()
            self.sftp = None
            if self.client:
                self.client.close()
            self.client = None
        except Exception as e:
            LOG.warning(f"Could not close ssh connection: {e}")

    def reconnect(self):
        """Reconnect SSH"""
        try:
            self.close()
            self.connect()
        except Exception as e:
            LOG.warning(f"Could not reconnect ssh: {e}")

    def get_filelist(self):
        """Return sorted files from current remote dir"""
        if self.client is None:
            self.connect()
        file_list = self.sftp.listdir()
        file_list.sort()
        return file_list

    def get_file(self, filename: str):
        """Get file from remote, store to workdir"""
        if self.client is None:
            self.connect()
        remotepath = f'{self.sftp.getcwd()}/{filename}'
        localpath = WORKDIR / filename
        LOG.info(f"Get file {filename} from remote server.. ")
        for attempt_no in range(2):
            try:
                self.sftp.get(
                    remotepath, localpath, callback=Connection.progress)
            except OSError as e:
                if attempt_no <= 2:
                    LOG.warning(
                        f"Could not get file. {e} \nAttempting reconnect.. ")
                    self.reconnect()
                    continue
                else:
                    raise ConnectionError(
                        "Could not get file due to connection error")

    @staticmethod
    def progress(transferred: int, tobe_transferred: int):
        """Return progress every x MB"""
        if Utils.convert_unit(transferred, SIZE_UNIT.MB) % 50 != 0:
            return
        sys.stdout.write(
            f"Transferred: "
            f"{Utils.convert_unit(transferred, SIZE_UNIT.GB):.2f}GB \t"
            f"out of: {Utils.convert_unit(tobe_transferred, SIZE_UNIT.GB):.2f}"
            f"GB".ljust(50) + "\r")
        sys.stdout.flush()
