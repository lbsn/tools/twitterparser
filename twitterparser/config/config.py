# -*- coding: utf-8 -*-

"""
Config module for parsing input args for twitterparser package.
"""

# pylint: disable=no-member

import os
import getpass
import argparse
import logging
from pathlib import Path
from typing import List, Tuple
from twitterparser import __version__


class BaseConfig():
    """Base config class for handling typical input params"""

    def __init__(self):
        self.debug = False
        self.workdir = Path.cwd() / "workdir"
        self.key_path: Path = None
        self.passphrase: str = None
        self.server_address: str = None
        self.username: str = None
        self.remote_directory: str = None
        self.lbsntransform_args = None
        self.remote_filelist: List[str] = None
        self.skip_filelist: List[str] = None
        self.zipped = False
        self.log = logging.getLogger("twitterparser")

    @staticmethod
    def get_arg_parser() -> argparse.ArgumentParser:
        """Define twitterparser cli arguments
        """
        parser = argparse.ArgumentParser()
        parser.add_argument('--version',
                            action='version',
                            version=f'twitterparser {__version__}')
        parser.add_argument('--debug',
                            help='Enable debug mode (verbose logging).',
                            action='store_true')
        parser.add_argument('-k', "--key_path",
                            help='Path to private SSH key.',
                            type=str)
        parser.add_argument('-s', "--server_address",
                            help='Server address to connect to via SSH.',
                            type=str)
        parser.add_argument('-u', "--username",
                            help='SSH username.',
                            type=str)
        parser.add_argument('-d', "--remote_directory",
                            help='Remote directory to process files from.',
                            required=True,
                            type=str)
        parser.add_argument('--zipped',
                            help='Use if remote files are zipped and must be '
                            'extracted before transformation. Default: False.',
                            action='store_true')
        return parser

    def parse_args(self):
        """Parse twitterparser args and forward remaining
        lbsntransform args"""
        parser = BaseConfig.get_arg_parser()
        args, remaining_args = parser.parse_known_args()
        # get private key passphrase first
        pk_env = os.environ.get('twitterparser_pk')
        if pk_env:
            self.passphrase = pk_env
        else:
            self.passphrase = getpass.getpass('SSH private key passphrase: ')
        if args.debug:
            self.debug = True
        if args.key_path:
            self.key_path = Path(
                args.key_path)
        if args.username:
            self.username = args.username
        if args.remote_directory:
            self.remote_directory = args.remote_directory
        if args.server_address:
            self.server_address = args.server_address
        if args.zipped:
            self.zipped = args.zipped
        self.lbsntransform_args = remaining_args

    def init_workdir(self):
        """Initialize workdir"""
        if self.workdir.exists():
            # check if list of files to process is
            # already present
            cfg_file = self.workdir / "filelist.cfg"
            if cfg_file.exists():
                self.log.info(f"{cfg_file.name} found.. continue processing.")
                self.remote_filelist = BaseConfig._load_filelist(cfg_file)
            # check if list of files to skip is
            # already present
            cfg_file = self.workdir / "skiplist.cfg"
            if cfg_file.exists():
                self.skip_filelist = BaseConfig._load_filelist(cfg_file)
                self.log.info(
                    f"Loaded list of {len(self.skip_filelist)} files to skip.")
        else:
            self.workdir.mkdir()
            print(f'Folder {self.workdir.name}/ was created')

    @staticmethod
    def _load_filelist(file: Path) -> List[str]:
        """Load a list of files from textfile"""
        with open(file, encoding="utf8") as f_handle:
            files = [line.rstrip() for line in f_handle]
        return files

    def set_remote_filelist(self, files: List[str]):
        """Set remote filelist and update cfg"""
        self.remote_filelist = files
        self.write_remote_filelist()

    def write_remote_filelist(self):
        """Write remote filelist to workdir"""
        cfg_file = self.workdir / "filelist.cfg"
        with open(cfg_file, 'w', encoding="utf-8") as f:
            for filename in self.remote_filelist:
                f.write(f"{filename}\n")

    def update_skiplist(self, file: str):
        """Update list of files to skip (skiplist.cfg)"""
        cfg_file = self.workdir / "skiplist.cfg"
        with open(cfg_file, 'a+', encoding="utf-8") as f:
            f.write(f"{file}\n")
